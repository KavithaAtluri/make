.ONESHELL:
.SHELL := /usr/bin/bash
.PHONY: one two

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[35m%-10s\033[0m %s\n", $$1, $$2}'	
one: ## Stage One
	@echo Helo One
two: one ## Stage Two
	@echo Hello two
	@sh echo.sh
